export type DatabaseSchema = {
  post: Post
  sub_state: SubState
}

export type Post = {
  uri: string
  cid: string
  replyParent: string | null
  replyRoot: string | null
  indexedAt: string
  labelPorn: string | null
  labelNudity: string | null
  // sexy: string | null
  // nude: string | null
  // exposedBreast: string | null
  // exposedBelly: string | null
  // exposedVagina: string | null
  // exposedButt: string | null
  // exposedPenis: string | null
  // exposedMaleBreast: string | null
  // exposedAnus: string | null
  // exposedArmpits: string | null
  // butt: string | null
  // feet: string | null
}

export type SubState = {
  service: string
  cursor: number
}
