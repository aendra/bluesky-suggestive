import { Kysely, Migration, MigrationProvider } from 'kysely'

const migrations: Record<string, Migration> = {}

export const migrationProvider: MigrationProvider = {
  async getMigrations() {
    return migrations
  },
}

migrations['001'] = {
  async up(db: Kysely<unknown>) {
    await db.schema
      .createTable('post')
      .addColumn('uri', 'varchar', (col) => col.primaryKey())
      .addColumn('cid', 'varchar', (col) => col.notNull())
      .addColumn('replyParent', 'varchar')
      .addColumn('replyRoot', 'varchar')
      .addColumn('indexedAt', 'varchar', (col) => col.notNull())
      .addColumn('labelPorn', 'varchar')
      .addColumn('labelNudity', 'varchar')
      .addColumn('sexy', 'varchar')
      .addColumn('nude', 'varchar')
      .addColumn('exposedBreast', 'varchar')
      .addColumn('exposedBelly', 'varchar')
      .addColumn('exposedVagina', 'varchar')
      .addColumn('exposedButt', 'varchar')
      .addColumn('exposedPenis', 'varchar')
      .addColumn('exposedMaleBreast', 'varchar')
      .addColumn('exposedAnus', 'varchar')
      .addColumn('exposedArmpits', 'varchar')
      .addColumn('butt', 'varchar')
      .addColumn('feet', 'varchar')
      .execute()
    await db.schema
      .createTable('sub_state')
      .addColumn('service', 'varchar', (col) => col.primaryKey())
      .addColumn('cursor', 'integer', (col) => col.notNull())
      .execute()
  },
  async down(db: Kysely<unknown>) {
    await db.schema.dropTable('post').execute()
    await db.schema.dropTable('sub_state').execute()
  },
}
