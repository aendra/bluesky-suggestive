import { AppBskyEmbedImages, BskyAgent } from '@atproto/api'
// import buttDetector from '../nudenet'
import {
  OutputSchema as RepoEvent,
  isCommit,
} from './lexicon/types/com/atproto/sync/subscribeRepos'
import { FirehoseSubscriptionBase, getOpsByType } from './util/subscription'
import debuglogger from 'debug'

const debug = debuglogger('suggestive:subscription')

const agent = new BskyAgent({ service: 'https://bsky.social' })
const MAX_AGE =
  (!Number.isNaN(process.env.MAX_AGE) && Number(process.env.MAX_AGE)) || 12
const SLEEP_TIME = 30000

interface IDBRecord {
  uri: string
  cid: string
  replyParent: string | null
  replyRoot: string | null
  indexedAt: string
}

export class FirehoseSubscription extends FirehoseSubscriptionBase {
  async handleEvent(evt: RepoEvent) {
    if (!isCommit(evt)) return
    const ops = await getOpsByType(evt)
    await agent.login({
      identifier: 'andi.hellthread.vet',
      password: process.env.USERPASS || '',
    })

    const postsToDelete = ops.posts.deletes.map((del) => del.uri)
    const postsToCreate = (
      await Promise.all<any[]>(
        ops.posts.creates
          // only image posts
          .filter(
            (create) =>
              create?.record?.embed?.images &&
              Array.isArray(create?.record?.embed?.images) &&
              create?.record?.embed?.images.length > 0 &&
              !create.record.text.includes('#private') && // filter any with #private in the body
              !create.record.text.includes('!no-promote'), // via https://github.com/bluesky-social/atproto/blob/a22d56aa21f1e21f187b76fb6a7b63b29e4f2e83/packages/pds/src/app-view/api/app/bsky/unspecced.ts#L11
          )
          // Process queue in parallel
          .map(async (create) => {
            try {
              const labels = await new Promise((res) =>
                setTimeout(res, SLEEP_TIME),
              ).then(() =>
                agent.app.bsky.feed
                  .getPosts({ uris: [create.uri] })
                  .then((d) => {
                    debug(d.data.posts)

                    // Always log mine
                    if (create.author === 'did:plc:oav2ixis42pm5fqrqhp4ey3w')
                      console.log('Andi post: ', d.data.posts)

                    return d.data.posts.flatMap(
                      (dd) => dd.labels?.map((l) => l.val) || [],
                    )
                  }),
              )

              debug(`labels`, labels)

              if (
                labels.some((label) =>
                  // via: https://github.com/bluesky-social/atproto/blob/a22d56aa21f1e21f187b76fb6a7b63b29e4f2e83/packages/pds/src/app-view/api/app/bsky/unspecced.ts#L16
                  ['sexual', 'porn', 'nudity', 'underwear'].includes(label),
                )
              ) {
                const newRecord = {
                  uri: create.uri,
                  cid: create.cid,
                  replyParent: create.record?.reply?.parent.uri ?? null,
                  replyRoot: create.record?.reply?.root.uri ?? null,
                  indexedAt: new Date().toISOString(),
                }
                console.log('New: ', newRecord)
                return newRecord
              }
            } catch (e) {
              console.error(e)
            }
          }),
      )
    ).filter((i) => i)

    if (postsToDelete.length > 0) {
      await this.db
        .deleteFrom('post')
        .where('uri', 'in', postsToDelete)
        .execute()
    }
    if (postsToCreate.length > 0) {
      await this.db
        .insertInto('post')
        .values(postsToCreate)
        .onConflict((oc) => oc.doNothing())
        .execute()
    }
    const oneHour = 60 * 60 * 1000 /* ms */
    const timeago = new Date(
      new Date().getTime() - MAX_AGE * oneHour,
    ).toISOString()
    await this.db.deleteFrom('post').where('indexedAt', '<', timeago).execute()
  }
}
