import * as tf from '@tensorflow/tfjs-node'

const options: IOptions = {
  // options
  debug: true,
  modelPath: 'file://nudenet/models/default-f16/model.json',
  minScore: 0.38,
  maxResults: 50,
  iouThreshold: 0.5,
  outputNodes: ['output1', 'output2', 'output3'],
  labels: [], // can be base or default
  classes: {
    // classes labels
    base: [
      'exposed belly',
      'exposed buttocks',
      'exposed breasts',
      'exposed vagina',
      'exposed penis',
      'male breast',
    ],
    default: [
      'exposed anus',
      'exposed armpits',
      'belly',
      'exposed belly',
      'buttocks',
      'exposed buttocks',
      'female face',
      'male face',
      'feet',
      'exposed feet',
      'breast',
      'exposed breast',
      'vagina',
      'exposed vagina',
      'male breast',
      'exposed penis',
    ],
  },
  composite: undefined, // can be base or default
  composites: {
    // composite definitions of what is a person, sexy, nude
    base: {
      person: [],
      sexy: [],
      nude: [2, 3, 4],
    },
    default: {
      person: [6, 7],
      sexy: [1, 2, 3, 4, 8, 9, 10, 15],
      nude: [0, 5, 11, 12, 13],
    },
  },
}

const models: any = {}

// read image file and prepare tensor for further processing
function getTensorFromImage(data: Uint8Array) {
  const bufferT = tf.node.decodeImage(data, 3)
  const expandedT = tf.expandDims(bufferT, 0)
  const imageT = tf.cast(expandedT, 'float32')

  tf.dispose([expandedT, bufferT])
  // if (options.debug)
  //   console.info('width:', imageT.shape[2], 'height:', imageT.shape[1])
  return imageT
}

// parse prediction data
async function processPrediction(
  boxesTensor: any,
  scoresTensor: any,
  classesTensor: any,
  inputTensor: any,
) {
  const boxes = await boxesTensor.array()
  const scores = await scoresTensor.data()
  const classes = await classesTensor.data()
  const nmsT = await tf.image.nonMaxSuppressionAsync(
    boxes[0],
    scores,
    options.maxResults,
    options.iouThreshold,
    options.minScore,
  ) // sort & filter results
  const nms = await nmsT.data()
  tf.dispose(nmsT)
  const parts: any[] = []
  for (const i in nms) {
    // create body parts object
    const id = parseInt(i)
    parts.push({
      score: scores[i],
      id: classes[id],
      class: options.labels[classes[id]],
      box: [
        // convert box from x0,y0,x1,y1 to x,y,width,heigh
        Math.trunc(boxes[0][id][0]),
        Math.trunc(boxes[0][id][1]),
        Math.trunc(boxes[0][id][3] - boxes[0][id][1]),
        Math.trunc(boxes[0][id][2] - boxes[0][id][0]),
      ],
    })
  }
  const result = {
    input: {
      file: inputTensor.file,
      width: inputTensor.shape[2],
      height: inputTensor.shape[1],
    },
    person:
      parts.filter((a) => options.composite.person.includes(a.id)).length > 0,
    sexy: parts.filter((a) => options.composite.sexy.includes(a.id)).length > 0,
    nude: parts.filter((a) => options.composite.nude.includes(a.id)).length > 0,
    parts,
  }
  if (options.debug) console.info('result:', result)
  return result
}

// load graph model and run inference
async function runDetection(input: any) {
  const t: any = {}
  if (!models[options.modelPath]) {
    // load model if not already loaded
    try {
      models[options.modelPath] = await tf.loadGraphModel(options.modelPath)
      models[options.modelPath].path = options.modelPath
      if (options.debug) console.info('loaded graph model:', options.modelPath)
      if (models[options.modelPath].version === 'v2.base') {
        options.labels = options.classes.base
        options.composite = options.composites.base
      } else {
        options.labels = options.classes.default
        options.composite = options.composites.default
      }
    } catch (err: any) {
      console.error(
        'error loading graph model:',
        options.modelPath,
        err.message,
        err,
      )
      return null
    }
  }
  t.input = getTensorFromImage(input) // get tensor from image
  ;[t.boxes, t.scores, t.classes] = await models[
    options.modelPath
  ].executeAsync(t.input, options.outputNodes) // run prediction

  const res = await processPrediction(t.boxes, t.scores, t.classes, t.input) // parse outputs

  Object.keys(t).forEach((tensor) => tf.dispose(t[tensor])) // free up memory

  return res
}

// main function
export default async function (buffer: Uint8Array) {
  await tf.enableProdMode()
  await tf.ready()
  return runDetection(buffer)
}

interface IOptions {
  debug: boolean
  modelPath: string
  minScore: number
  maxResults: number
  iouThreshold: number
  outputNodes: string[]
  labels: string[] // can be base or default
  classes: {
    // classes labels
    base: string[]
    default: string[]
  }
  composite?: any // can be base or default
  composites: any
}
